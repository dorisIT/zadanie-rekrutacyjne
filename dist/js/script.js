$(function() {
    $(".rslides").responsiveSlides({
      auto: true,
      speed: 500,
      timeout: 5000, 
    });

    window.onscroll = function() {stickyHeader()};
    var navbar = $("header");
    
    function stickyHeader() {
      if (window.pageYOffset >= 90) {
        navbar.addClass('header-sticky');
      } else {
        navbar.removeClass('header-sticky');
      }
    }
    stickyHeader();

    $('#accordionExample').on('shown.bs.collapse', function (e) {
      $(e.target).prev().addClass('accordion-active');
    });
  
    $('#accordionExample').on('hidden.bs.collapse', function (e) {
      $(e.target).prev().removeClass('accordion-active');
    });

    $('#cards-box-load').on('click', function() {
      $.ajax({
        url: '/tiles.html',
        type:'GET',
        success: function(data){
          var placeholder = $('#cards-box-placeholder');
          var button = $('#cards-box-load');

          button.addClass('d-none');
          placeholder.html(data);
        }
     });
    });

    $('a.tBlank').each(function(index, element) {
      $(element).attr('target', '_blank');
    });
  });



